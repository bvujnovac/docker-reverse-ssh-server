#!/bin/bash

if [ "$1" == "" ]
then
  echo "Usage: node-connect <device_uid> "
  echo "To list available devices use: node-connect list"
  exit
fi

if [ "$1" == "list" ]
then
    echo "Listing all accessible devices"
    ls -l $HOME/devices/| awk '{print $9}'| grep -v "\."
    echo " "
    exit
fi

MY_NODE_UUID=$1
DEVICE_HOME=~/devices/$MY_NODE_UUID
SERVER_PORT=10000

if [ ! -d $DEVICE_HOME ]
then
  echo "Device home dir does not exist: $DEVICE_HOME"
  exit
fi

if [ ! -f $DEVICE_HOME/home-msg ]
then
  echo "1" > $DEVICE_HOME/home-msg
  if [ -f ~/.ssh/known_hosts ]
  then
    rm -rf ~/.ssh/known_hosts
  fi
fi
# Loop until msg is removed
while :
do
  [[ ! -f $DEVICE_HOME/home-msg ]] && break
  echo "Waiting until device connects"
  sleep 5
done

echo "Connecting to device $MY_NODE_UUID on server port $SERVER_PORT"
sleep 3
ssh -o "StrictHostKeyChecking no" -i ~/ssh_host_rsa_key -p$SERVER_PORT root@localhost
