#!/bin/bash

MY_NODE_UUID=$1
DEVICE_HOME=~/devices/$MY_NODE_UUID

if [ ! -d $DEVICE_HOME ]
then
#  echo "Device  home dir does not exist: $DEVICE_HOME"
#  echo "creating home dir for $MY_NODE_UUID"
  mkdir -p $DEVICE_HOME
fi

if [ -f $DEVICE_HOME/home-msg ]
then
    cat $DEVICE_HOME/home-msg
    rm $DEVICE_HOME/home-msg
else
    echo "0"
fi
